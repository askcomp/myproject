﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public static Parking instance = null;
        private decimal balance;
        public List<Vehicle> vehicle;

        protected Parking()
        {
            balance = Settings.initialParkingBalance;
            vehicle = new List<Vehicle>(Settings.parkingCapacity);
        }

        // implement Singletone
        public static Parking GetInstane()
        {
            if (instance == null)
            {
                instance = new Parking();
                return instance;
            }
            return instance;
        }


        public decimal Balance
        {
            get => balance;
        }

        public void TopUp(decimal sum)
        {
            if (sum > 0)
            {
                balance += sum;
            }
        }
    }
}