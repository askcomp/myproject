﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        private string transactionTime;
        private string vehicleId;
        private decimal vehicleBalance;
        private decimal Sum;

        public TransactionInfo(string transactionTime, string vehicleId, decimal vehiclebalance, decimal Sum)
        {
            this.transactionTime = transactionTime;
            this.vehicleId = vehicleId;
            this.vehicleBalance = vehiclebalance;
            this.Sum = Sum;
        }

        public string TransactionTime { get => transactionTime; }
        public string VehicleId { get => vehicleId; }
        public decimal VehicleBalance { get => vehicleBalance; }
        public decimal WithdrawSum { get => Sum; }
    }
}