﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Models;
using CoolParking.BL.Services;


namespace CoolParking.BL
{
    class Program
    {
        static void Main(string[] args)
        {
            ParkingService parkingService = new ParkingService();
            
            var CoolParkingMenu = new Menu();
            Console.WriteLine("Welcome to the Cool Parking!");

            bool state = true;
            while (state)
            {
                switch (Menu.ShowMenu())
                {
                    case 1:
                        CoolParkingMenu.CurrentBalanceMenu();
                        break;
                    case 2:
                        CoolParkingMenu.AmountMenu();
                        break;
                    case 3:
                        CoolParkingMenu.FreeOccupiedMenu();
                        break;
                    case 4:
                        //CoolParkingMenu.GetLastParkingTransactionsMenu();
                        parkingService.AddVehicle(new Vehicle(VehicleType.PassengerCar, 10));
                        break;
                    case 5:
                        CoolParkingMenu.ReadFromLogMenu();
                        break;
                    case 6:
                        CoolParkingMenu.GetVehiclesMenu();
                        break;
                    case 7:
                        CoolParkingMenu.AddVenicleMenu();
                        break;
                    case 8:
                        CoolParkingMenu.RemoveVehicleMenu();
                        break;
                    case 9:
                        CoolParkingMenu.TopUpVehicleMenu();
                        break;
                    case 0:
                        state = false;
                        break;
                    default:
                        Menu.ShowMenu();
                        break;
                }
            }
            

            Console.ReadKey();
        }
    }


}
