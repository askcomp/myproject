﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using System;
using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        private string logPath;

        public string LogPath
        {
            get => logPath;
            private set
            {
                if (value != null)
                {
                    logPath = value;
                }
            }
        }

        public LogService(string logPath)
        {
            this.LogPath = logPath;
        }

        public string Read()
        {
            string readedLogInfo = null;
            try
            {
                using (StreamReader streamReader = new StreamReader(logPath))
                {
                    if (streamReader == null)
                    {
                        throw new InvalidOperationException();
                    }
                    readedLogInfo = streamReader.ReadToEnd();
                    streamReader.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot read the file");
                Console.WriteLine(e.Message);
            }
            return readedLogInfo;
        }

        public void Write(string logInfo)
        {
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(logPath, true))
                {
                    streamWriter.WriteLine(logInfo);
                    streamWriter.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot write into the file");
                Console.WriteLine(e.Message);
            }
    
        }
    }

}