﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService

    {
        private Parking parking;
        private TimerService withdrawTimer;
        private TimerService logTimer;
        private LogService logService;
        List<TransactionInfo> currentTransactionInfo;


        public ParkingService()
        {
            Parking.GetInstane();
            parking = Parking.instance;
            logService = new LogService(Settings.logFilePath);
            currentTransactionInfo = new List<TransactionInfo>();
            withdrawTimer = new TimerService(Settings.withdrawIntervalInMillis, RegularWithdraw);
            logTimer = new TimerService(Settings.logIntervalInMillis, LogCurrentTransactions);
            withdrawTimer.Start();
            logTimer.Start();
        }

        private void LogCurrentTransactions(object sender, ElapsedEventArgs e)
        {
            logService.Write(FormateTransactionInfo());

            string FormateTransactionInfo()
            {
                TransactionInfo[] currentTransactionInfo = GetLastParkingTransactions();
                string logFileInfo = "";
                foreach (TransactionInfo t in currentTransactionInfo)
                {
                    logFileInfo += "\n" + t.TransactionTime + "\n";
                    logFileInfo += "Id: " + t.VehicleId + "\n";
                    logFileInfo += "Balance: " + t.VehicleBalance + "\n";
                    logFileInfo += "Withdrawn sum: " + t.WithdrawSum + "\n";
                }
                return logFileInfo;
            }

        }

        public void AddVehicle(Vehicle vehicle)
        {
            foreach (Vehicle item in parking.vehicle)
            {
                if (item.Id == vehicle.Id)
                    throw new InvalidOperationException();
            }

            parking.vehicle?.Add(vehicle);
        }


        public void RemoveVehicle(string vehicleId)
        {
            parking.vehicle.Remove(FindVehicleById(vehicleId));
        }

        private Vehicle FindVehicleById(string id)
        {
            foreach (Vehicle item in parking.vehicle)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            throw new InvalidOperationException();

        }

        private void RegularWithdraw(object sender, ElapsedEventArgs e)
        {
            foreach (Vehicle item in parking.vehicle)
            {
                decimal withdrawnSum = CalculateWithdrawnSum(item);
                item.Withdraw(withdrawnSum);
                currentTransactionInfo.Add(ComposeTransactionInfo(item, withdrawnSum));
                Console.WriteLine("{1} was withdrawn from the vehicle with id {0}. Vehicle balance is {2}", item.Id, withdrawnSum, item.Balance);
            }
            //Console.WriteLine(Environment.NewLine);

            TransactionInfo ComposeTransactionInfo(Vehicle vehicle, decimal withdrawnSum)
            {
                TransactionInfo transactionInfo = new TransactionInfo(
                    DateTime.Now.ToShortTimeString() + ' ' + DateTime.Now.ToShortDateString(),
                    vehicle.Id,
                    vehicle.Balance,
                    withdrawnSum
                    );
                return transactionInfo;
            }

        }



        private decimal CalculateWithdrawnSum(Vehicle vehicle)
        {
            decimal withdrawnSum;
            Settings.vehicleParkingTariff.TryGetValue(vehicle.VehicleType, out withdrawnSum);
            if (vehicle.Balance >= withdrawnSum)
            {
                return withdrawnSum;
            }
            else if (vehicle.Balance == 0)
            {
                return withdrawnSum * Settings.penaltyMultiplier;
            }
            else if (vehicle.Balance < withdrawnSum && vehicle.Balance > 0)
            {
                return (withdrawnSum - vehicle.Balance) * Settings.penaltyMultiplier + (withdrawnSum - vehicle.Balance);
            }
            else
            {
                return Math.Abs(vehicle.Balance) * Settings.penaltyMultiplier;
            }
        }

        public void Dispose()
        {
            withdrawTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return parking.Balance;
        }

        public int GetCapacity()
        {
            return parking.vehicle.Capacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parking.vehicle.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return currentTransactionInfo?.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            ReadOnlyCollection<Vehicle> vehicles = new ReadOnlyCollection<Vehicle>(parking.vehicle);
            return vehicles;
        }

        public string ReadFromLog()
        {
            StreamReader streamReader = new StreamReader(logService.LogPath);
            return streamReader.ReadToEnd();
        }


        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            FindVehicleById(vehicleId).TopUp(sum);
        }
    }

}