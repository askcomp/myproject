﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System;
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;

        public event ElapsedEventHandler Elapsed;

        public double Interval
        {
            get => timer.Interval;
            set
            {
                if (value > 0)
                {
                    timer.Interval = value;
                }
                else throw new ArgumentException();
            }
        }

        public TimerService(double interval, ElapsedEventHandler eventHandler)
        {
            timer = new Timer();
            Interval = interval;
            if (eventHandler != null)
            {
                timer.Elapsed += eventHandler;
            }
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

        public void Dispose()
        {
            timer.Dispose();
        }

    }
}